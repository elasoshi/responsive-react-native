/* @flow weak */

import React from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

import { Title } from '../Common/Index'
import Icon from 'react-native-vector-icons/FontAwesome';

import { widthPercentageToDP, heightPercentageToDP } from '../../Responsive'

const DefaultSaveCard = ({ icon, title, text, buttonText }) => (
  <View style={styles.container}>

    <View style={styles.inner}>

      <View style={styles.logo}>
        <Icon name={icon} size={120} color="white" />
      </View>

      <View style={styles.textCon}>
        <Text style={styles.text1}>{title}</Text>
      </View>

      <View style={styles.textCon2}>
        <Text style={styles.text2}>{text}</Text>
      </View>

      <View style={styles.buttonCon}>
        <View style={styles.buttonStyle}>
          <Text style={styles.text3}>{buttonText}</Text>
        </View>
      </View>




    </View>

  </View>
);

export { DefaultSaveCard };

const styles = StyleSheet.create({
  container: {
    width: widthPercentageToDP('100%'),
    height: heightPercentageToDP('70%'),
    backgroundColor: 'white',
    alignItems: 'center'
  },
  inner: {
    width: widthPercentageToDP('90%'),
    height: heightPercentageToDP('70%'),
    alignItems: 'center'
  },
  text1: {
    fontSize: 19,
  fontWeight: "bold",
  fontStyle: "normal",
  letterSpacing: 0,
  textAlign: "center",
  color: "#252729",
  fontFamily: "MaisonNeue-Bold",
  },
  text2: {
    fontFamily: "MaisonNeue-Book",
  fontSize: 15,
  fontWeight: "500",
  fontStyle: "normal",
  letterSpacing: 0,
  textAlign: "center",
  color: "#4d4d4d"
  },
  logo: {
    width: widthPercentageToDP('60%'),
    height: widthPercentageToDP('60%'),
    borderRadius: widthPercentageToDP('60%')/2,
    backgroundColor: '#f1f3f4',
    justifyContent: 'center',
    alignItems: 'center'

  },
  textCon: {
    height: widthPercentageToDP('20%'),
    justifyContent: 'center'

  },
  textCon2: {
    width: widthPercentageToDP('90%'),
    justifyContent: 'center'

  },
  buttonStyle: {
    borderRadius: 28.5,
    backgroundColor: "#00848a",
    width: widthPercentageToDP('40%'),
    height: heightPercentageToDP('7%'),
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonCon: {
    height: heightPercentageToDP('20%'),
    justifyContent: 'center'
  },
  text3: {
    fontFamily: "MaisonNeue-Book",
  fontSize: 17,
  fontWeight: "500",
  fontStyle: "normal",
  letterSpacing: 0,
  textAlign: "center",
  color: "#ffffff"
  }
});
