/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

import { Header, Title, SubTitle, Input, Button, TextButton } from '../Components'

export default class Login extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Header onPress={() => this.props.navigation.goBack()}/>
        <Title align={true}>login your acount</Title>
        <SubTitle align={true} size={13}>Simply enter your number</SubTitle>
        <Input
          label='Enter your number'
        />
        <Button
          color={['#2462e8','#37e0ff']}
          shadowColor='rgba(20, 185, 254, 0.2)'
          onPress={() => this.props.navigation.navigate('OtpScreen')}
          >
          NEXT
        </Button>

        <TextButton>Reset Password</TextButton>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
});
